package com.elearning.MyElearningWebsite.Repository;

import com.elearning.MyElearningWebsite.Model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {

}
