package com.elearning.MyElearningWebsite.Model;

public enum Category {
    INFORMATICS,
    MATHEMATICS,
    ENGLISH,
    DATA
}
