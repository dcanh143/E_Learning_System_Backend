package com.elearning.MyElearningWebsite.Model;

public enum Role {
    STUDENT,
    TEACHER
}
