package com.elearning.MyElearningWebsite.Service;


import com.elearning.MyElearningWebsite.Model.User;

public interface AuthenticationService {
    User SignInAndReturnJWT(User signInRequest);
}
