package com.elearning.MyElearningWebsite.Service;



import com.elearning.MyElearningWebsite.Model.Role;
import com.elearning.MyElearningWebsite.Model.User;

import java.util.Optional;

public interface UserService {
    User saveUser(User user);

    Optional<User> findByUsername(String username);

}
