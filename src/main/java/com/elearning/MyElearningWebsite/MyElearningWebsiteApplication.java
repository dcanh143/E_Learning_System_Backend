package com.elearning.MyElearningWebsite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyElearningWebsiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyElearningWebsiteApplication.class, args);
	}

}
